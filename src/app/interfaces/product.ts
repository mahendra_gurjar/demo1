export interface Product {
	id:number;
	name:string;
	short_description:string;
	price:number;
	store_price:number;
}
