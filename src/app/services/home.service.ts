import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HomeService {

  constructor(private http: HttpClient) { }

  getProduct(){
  	console.log('In home service');
  	return this.http.get('http://intelliasolutions.com/demo/ecommerce1/api/getProductByCategory?venderId=2&categoryId=3')
	    .map(res => res);
	}
}
