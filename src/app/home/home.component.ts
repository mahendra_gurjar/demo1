import { Component, OnInit } from '@angular/core';
import { HomeService } from '../services/home.service';
import { Product } from '../interfaces/product';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
	products : Product[];
  constructor(private HomeService:HomeService) { }

  ngOnInit() {
  	this.HomeService.getProduct().subscribe((products)=>{
		this.products = products.data;
		console.log(this.products);
	});
  }

}
